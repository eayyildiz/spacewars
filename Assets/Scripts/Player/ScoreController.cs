﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    public Text scoreText;
    public Text healthText;

    public static ScoreController singleton;

    int score = 0;
    int lives = 3;

    public void Awake()
    {
        singleton = this;
    }

    public void AddScore(int point)
    {
        score += point;
        UpdateScore();
    }

    public void DropHealth()
    {
        lives -= 1;
        UpdateHealth();
    }

    public int GetHealth()
    {
        return lives;
    }

    void UpdateHealth()
    {
        healthText.text = $"{lives}";
    }

    void UpdateScore() 
    {
        scoreText.text = $"{score}";
    }
}
