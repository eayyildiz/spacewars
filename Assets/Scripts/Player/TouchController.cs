﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    private PlayerController controller;
    // Start is called before the first frame update
    void Awake()
    {
        controller = gameObject.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touches.Length; i++)
            {
                Touch touch = Input.GetTouch(i);
                if (touch.position.x > Screen.width / 2)
                {
                    controller.Fire();
                }
                else if (touch.position.y < Screen.height / 2)
                {
                    controller.MoveUp();
                }
                else
                {
                    controller.MoveDown();
                }
            }
        }
    }
}
