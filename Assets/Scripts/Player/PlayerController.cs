﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    public float minY, maxY;
    public float attackTimer = 0.35f;
    private float currentAttackTimer;
    private bool canAttack;

    [SerializeField]
    private GameObject playerBullet;

    [SerializeField]
    private Transform attackPoint;

    private AudioSource laserAudio;

    void Awake()
    {
        laserAudio = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentAttackTimer = attackTimer;
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
        Attack();
    }

    void MovePlayer()
    {
        if (Input.GetAxisRaw("Vertical") > 0f)
        {
            MoveUp();
        }
        else if (Input.GetAxisRaw("Vertical") < 0f)
        {
            MoveDown();
        }
    }

    public void MoveUp()
    {
        Vector3 temp = transform.position;
        temp.y += speed * Time.deltaTime;

        if (temp.y > maxY)
            temp.y = maxY;

        transform.position = temp;
    }

    public void MoveDown()
    {
        Vector3 temp = transform.position;
        temp.y -= speed * Time.deltaTime;

        if (temp.y < minY)
            temp.y = minY;

        transform.position = temp;
    }

    void Attack()
    {
        attackTimer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.K))
        {
            Fire();
        }
    }

    public void Fire()
    {
        if (attackTimer > currentAttackTimer)
        {
            canAttack = true;
        }

        if (canAttack)
        {
            canAttack = false;
            attackTimer = 0f;
            Instantiate(playerBullet, attackPoint.position, Quaternion.identity);
            laserAudio.Play();

        }
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if ((target.tag == "Bullet") || target.tag == "Enemy")
        {
            ScoreController.singleton.DropHealth();

            if (ScoreController.singleton.GetHealth() <= 0)
            {
                SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
            }
            else
            {
                ResetPlayer();
            }
        }
    }

    void ResetPlayer()
    {
        Vector3 temp = transform.position;
        temp.y = 0;
        transform.position = temp;

        gameObject.GetComponent<Collider2D>().enabled = false;

        Invoke("ReactivatePlayer", 2f);
    }

    void ReactivatePlayer() 
    {
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
}
