﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public float speed = 5f;
    public float deactivateTimer = 3.5f;

    [HideInInspector]
    public bool isEnemyBullet = false;

    void Start()
    {
        Invoke("DeactivateGameObject", deactivateTimer);
        if (isEnemyBullet) {
            speed *= -1f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        Vector3 temp = transform.position;
        temp.x += speed * Time.deltaTime;
        
        transform.position = temp;
    }

    void DeactivateGameObject()
    {
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Bullet" || (!isEnemyBullet && target.tag == "Enemy"))
        {
            gameObject.SetActive(false);
        }
    }
}
