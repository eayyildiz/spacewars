﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float minY = -4.3f, maxY = 4.3f;
    public GameObject[] asteroidPrefabs;
    public GameObject[] enemyPrefabs;
    public float timer = 2f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("SpawnEnemies", timer);
    }

    void SpawnEnemies()
    {
        float posY = Random.Range(minY, maxY);
        Vector3 temp = transform.position;
        temp.y = posY;

        if(Random.Range(0,2) > 0) {
            Instantiate(asteroidPrefabs[Random.Range(0, asteroidPrefabs.Length)],
                temp,
                Quaternion.identity);
        }
        else {
            Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)],
                temp,
                Quaternion.identity);
        }

        Invoke("SpawnEnemies", timer);
    }
}
