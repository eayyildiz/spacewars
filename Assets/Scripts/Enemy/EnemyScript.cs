﻿using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public float speed = 5f;
    public float rotateSpeed = 50f;
    public int reward = 50;

    public bool canShoot;
    public bool canRotate;
    private bool canMove = true;

    public float boundX = -11f;
    public Transform attackPoint;
    public GameObject bulletPrefab;

    private Animator animator;
    private AudioSource explosionSound;

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();
        explosionSound = GetComponent<AudioSource>();
    }

    void Start()
    {
        if (canRotate) { 
            if(Random.Range(0,2)> 0) {
                rotateSpeed = Random.Range(rotateSpeed, rotateSpeed + 20f);
                rotateSpeed *= -1f;
            }
        }

        if (canShoot) {
            Invoke("StartShooting", Random.Range(1f, 3f));
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        RotateEnemy();
    }

    void Move()
    {
        if (canMove) {
            Vector3 temp = transform.position;
            temp.x -= speed * Time.deltaTime;
            transform.position = temp;

            if(temp.x < boundX) {
                gameObject.SetActive(false);
            }
        }
    }

    void RotateEnemy()
    {
        if (canRotate)
        {
            transform.Rotate(new Vector3(0f, 0f, rotateSpeed * Time.deltaTime), Space.World);
        }
    }

    void StartShooting() 
    {
        GameObject bullet = Instantiate(bulletPrefab, attackPoint.position, Quaternion.identity);
        bullet.GetComponent<BulletScript>().isEnemyBullet = true;

        if (canShoot)
        {
            Invoke("StartShooting", Random.Range(1f, 3f));
        }
    }

    void TurnOffGameObject() 
    {
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D target) 
    { 
        if(target.tag == "Bullet") 
        {
            canMove = false;
            if (canShoot)
            {
                canShoot = false;
                CancelInvoke("StartShooting");
            }
            gameObject.GetComponent<Collider2D>().enabled = false;
            Invoke("TurnOffGameObject", 1f);

            animator.Play("Destroy");
            ScoreController.singleton.AddScore(reward);
            explosionSound.Play();
        }
    }
}
