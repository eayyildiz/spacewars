﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("ReturnMenu", 3f);
    }

    void ReturnMenu() {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

}
